﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Question2.Models;
using System.Threading;

namespace Question2.Console
{
    class Program
    {
        #region Main
        static void Main(string[] args)
        {
            ///Welcome
            System.Console.WriteLine();
            System.Console.WriteLine("Daves game of life");
            System.Console.WriteLine();

            ///Get number of columns
            string input;
            int numberOfColumns = GetWholeNumberInput("Enter the number of columns");

            ///Get number of rows
            int numberOfRows = GetWholeNumberInput("Enter the number of rows");

            ///Get number of generations
            int numberOfGenerations = GetWholeNumberInput("Enter the number of generations");

            ///Create life grid
            int[,] newGrid = LifeGrid.PopulateGridWithRandomValues(numberOfRows, numberOfColumns);

            LifeGrid lifeGrid = new LifeGrid(newGrid);

            ///Draw generations
            while (lifeGrid.AliveCells() > 0 &&
                    lifeGrid.GenerationCount < numberOfGenerations)
            {
                lifeGrid.ProcessGeneration();

                System.Console.Clear();
                System.Console.WriteLine(lifeGrid.DrawGeneration());
                System.Console.WriteLine();
                System.Console.WriteLine("Generation {0}", lifeGrid.GenerationCount);

                if (lifeGrid.AliveCells() == 0)
                {
                    System.Console.WriteLine("Every one died!");
                    System.Console.ReadLine();

                    ///The end
                    return;
                }

                ///Add slight pause to prevent blinking in consol output updates
                Thread.Sleep(250);
            }

            ///Number of generations limit reached
            System.Console.WriteLine("Maximum generations reached!");
            System.Console.ReadLine();

            ///The end
            return;
        }
        #endregion

        #region Private Methods
        private static int GetWholeNumberInput(string instructions)
        {
            int result = 0;
            string input;

            do
            {
                ///Ask user for input
                System.Console.WriteLine();
                System.Console.WriteLine(instructions);
                input = System.Console.ReadLine();

                ///Validate input
                if (input != null && Validations.IsInputAnIntGreaterThanZero(input))
                {
                    result = int.Parse(input);
                }
                else
                {
                    ///Invalid input
                    System.Console.WriteLine("Invalid entry, please try again");
                }
            } while (result == 0);

            return result;
        }
        #endregion

    }
}
