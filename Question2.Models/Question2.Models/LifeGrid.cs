﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2.Models
{
    public class LifeGrid
    {
        #region Private Fields

        int[,] currentGeneration;
        int[,] lastGeneration;

        int generationCount;

        int width;
        int height;
        #endregion

        #region Public Properties
        public int GenerationCount
        {
            get { return generationCount; }
        }
        #endregion


        #region Constructor

        public LifeGrid(int[,] newGrid)
        {
           

            ///Initiase grid
            currentGeneration = (int[,])newGrid.Clone();

            generationCount = 0;

            width = currentGeneration.GetLength(1);
            height = currentGeneration.GetLength(0);

            lastGeneration = new int[height, width];
        }
        #endregion




      


        #region Private Methods

        private int Neighbours(int x, int y)
        {
            int count = 0;

            // Check for x - 1, y - 1
            if (x > 0 && y > 0)
            {
                if (currentGeneration[y - 1, x - 1] == 1)
                    count++;
            }

            // Check for x, y - 1
            if (y > 0)
            {
                if (currentGeneration[y - 1, x] == 1)
                    count++;
            }

            // Check for x + 1, y - 1
            if (x < width - 1 && y > 0)
            {
                if (currentGeneration[y - 1, x + 1] == 1)
                    count++;
            }

            // Check for x - 1, y
            if (x > 0)
            {
                if (currentGeneration[y, x - 1] == 1)
                    count++;
            }

            // Check for x + 1, y
            if (x < width - 1)
            {
                if (currentGeneration[y, x + 1] == 1)
                    count++;
            }

            // Check for x - 1, y + 1
            if (x > 0 && y < height - 1)
            {
                if (currentGeneration[y + 1, x - 1] == 1)
                    count++;
            }

            // Check for x, y + 1
            if (y < height - 1)
            {
                if (currentGeneration[y + 1, x] == 1)
                    count++;
            }

            // Check for x + 1, y + 1
            if (x < width - 1 && y < height - 1)
            {
                if (currentGeneration[y + 1, x + 1] == 1)
                    count++;
            }

            return count;
        }


        #endregion

        #region Public Static
        public static int[,] PopulateGridWithRandomValues(int numberOfRows, int numberOfColumns)
        {
            Random rnd = new Random();

            int[,] result = new int[numberOfRows, numberOfColumns];

            for (int rowCounter = 0; rowCounter < numberOfRows; rowCounter++)
            {
                for (int columnCounter = 0; columnCounter < numberOfRows; columnCounter++)
                {
                    ///Create random bool value
                    result[rowCounter, columnCounter] = rnd.Next(2);
                }
            }

            return result;
        }
        #endregion

        #region Public Methods

        public void ProcessGeneration()
        {
            int[,] nextGeneration = new int[height, width];

            ///Copy current generation to last generation
            lastGeneration = (int[,])currentGeneration.Clone();

            generationCount++;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    ///If there was a dead cell that had exactly three neighbours a new cell would be born
                    if (currentGeneration[y, x] == 0 && Neighbours(x, y) == 3)
                    {
                        nextGeneration[y, x] = 1;
                    }

                    ///If a cell was alive and had two or three neighbours the cell would survive.
                    if (currentGeneration[y, x] == 1 &&
                           (Neighbours(x, y) == 2 || Neighbours(x, y) == 3))
                    {
                        nextGeneration[y, x] = 1;
                    }
                }
            }

            currentGeneration = (int[,])nextGeneration.Clone();
        }

        public string DrawGeneration()
        {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                    sb.AppendFormat("{0}", currentGeneration[y, x]);
                sb.Append(Environment.NewLine);
            }
            sb.Append(Environment.NewLine);

            string result = sb.ToString();

            return result;
        }

        public int AliveCells()
        {
            int count = 0;

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                    if (currentGeneration[y, x] == 1)
                        count++;

            return count;
        }





        #endregion
    }
}
