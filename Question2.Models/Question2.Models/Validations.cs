﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2.Models
{
    public class Validations
    {
        public static bool IsInputAnIntGreaterThanZero(string input)
        {
            int number;
            bool isInt = Int32.TryParse(input, out number);
            if (isInt)
            {
                if (number > 0)
                {
                    return true;
                }
            }

            return false;

        }
    }
}
