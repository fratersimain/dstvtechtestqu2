﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question2.Models;

namespace Question2.Tests
{
    [TestClass]
    public class LifeGridTests
    {
        int[,] _newGrid;

        #region Contructor
        public LifeGridTests()
        {
            ///Init all test values
            ///Define test grid
            _newGrid = new int[,]
	{
		{ 1, 0, 0, 0, 0, 0, 0, 0, 1, 1,},
		{ 0, 0, 1, 0, 0, 1, 0, 0, 0, 0,},
		{ 0, 1, 1, 0, 1, 1, 1, 0, 0, 0,},
		{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0,},
		{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 1,},
		{ 0, 0, 0, 0, 1, 0, 0, 0, 1, 1,},
		{ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,},
		{ 1, 1, 0, 0, 1, 0, 0, 0, 0, 0,},
	};
        }
        #endregion

        [TestMethod]
        public void PopulateGridWithRandomValuesTest()
        {
            ///Arrange
            int numberOfRows = 4;
            int numberOfColumns = 8;

            ///Act
            int[,] newGrid = LifeGrid.PopulateGridWithRandomValues(numberOfRows, numberOfColumns);

            ///Assert
            ///Check number of dimensions in array
            Assert.IsTrue(newGrid.Rank == 2);

            ///Check the total number of cells
            Assert.IsTrue(newGrid.Length == numberOfRows * numberOfColumns);

            ///Check the number of columns
            int resultNumberOfColumns = newGrid.GetUpperBound(1) - newGrid.GetLowerBound(1) + 1;

            Assert.IsTrue(resultNumberOfColumns == numberOfColumns);

        }

        [TestMethod]
        public void AliveCellsTest()
        {
            ///Arrange
            LifeGrid lifeGrid = new LifeGrid(_newGrid);

            ///Act
            int result = lifeGrid.AliveCells();

            ///Assert
            Assert.IsTrue(result == 23);
        }

        [TestMethod]
        public void DrawGenerationTest()
        {
            ///Arrange
            LifeGrid lifeGrid = new LifeGrid(_newGrid);

            ///Act
            string result = lifeGrid.DrawGeneration();

            ///Assert
            Assert.IsTrue(result == "1000000011\r\n0010010000\r\n0110111000\r\n0100010000\r\n0000000101\r\n0000100011\r\n0100100100\r\n1100100000\r\n\r\n");
        }

        [TestMethod]
        public void ProcessGenerationTest()
        {
            ///Arrange
            LifeGrid lifeGrid = new LifeGrid(_newGrid);

            ///Act
            lifeGrid.ProcessGeneration();
            string result = lifeGrid.DrawGeneration();

            ///Assert
            Assert.IsTrue(result == "0000000000\r\n0011111000\r\n0111101000\r\n0110110000\r\n0000000001\r\n0000000101\r\n1101110010\r\n1100000000\r\n\r\n");
        }


        [TestMethod]
        public void ValidationWholeNumberTest()
        {
            ///Arrange
            string goodInput = "5";
            string badInput1 = "5.5";
            string badInput2 = "abc";
            string badInput3 = "-5";

            ///Act
            bool result1 = Validations.IsInputAnIntGreaterThanZero(goodInput);

            bool result2 = Validations.IsInputAnIntGreaterThanZero(badInput1);

            bool result3 = Validations.IsInputAnIntGreaterThanZero(badInput2);

            bool result4 = Validations.IsInputAnIntGreaterThanZero(badInput3);

            ///Assert
            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
            Assert.IsFalse(result3);
            Assert.IsFalse(result4);
        }
    }
}
